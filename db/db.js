/**
 * Created by thedafda on 07/07/15.
 */

var Promise = require('bluebird'),
    MongoDB = require('mongodb'),
    Base62 = require('../base62/base62');

Promise.promisifyAll(MongoDB);
var MONGO_URL = process.env.MONGO_URL || 'mongodb://jdaf:jdaf@ds047642.mongolab.com:47642/urls-map';
exports.connect = function(){
    return MongoDB.MongoClient.connect(MONGO_URL).then(function(db){
        return db.collection('urls').createIndex({
            url: 1
        }).then(function(){
            return new DbWrapper(db);
        });
    })
};

function DbWrapper(db){
    this.db = db;
    this.urlCollection = db.collection('urls');
    this.counterCollection = db.collection('counters');
}

DbWrapper.prototype.urlExist = function (longUrl) {
    return this.urlCollection.findAsync({
        url: { $eq: longUrl }
    }).then(function(cursor){
        return cursor.toArray();
    }).then(function(data) {
        if (data.length > 0) {
            return data[0];
        }
    });
};

DbWrapper.prototype.getUrlObjectById = function (id) {
    return this.urlCollection.findAsync({
            _id: id
        }).then(function(cursor){
            return cursor.toArray();
        }).then(function(arr){
            return arr && arr[0];
        });
};

DbWrapper.prototype.counter = function (name) {
    return this.counterCollection.findAndModify(
        {
            id: name
        },
        [],
        {
            $inc : {
                next: 1
            }
        },
        {
            "new": true,
            "upsert": true
        }
    );
};

DbWrapper.prototype.getShortUrl = function(longUrl, options){
    var scope = this;
    options = options || {};
    options.baseDomain = options.baseDomain || 'http://localhost:9000/';

    return scope.urlExist(longUrl).then(function(urlObject) {
         if(urlObject){
            return urlObject;
         }
         return scope.counter('urls')
            .then(function (ret) {
                return {
                    _id: Number(ret.value.next),
                    url: longUrl
                };
            }).then(function (urlObject) {
                urlObject.hash = Base62.encode(urlObject._id);
                urlObject.shortUrl = options.baseDomain + 'go/' + urlObject.hash;
                return scope.urlCollection.insert(urlObject).then(function () {
                    return urlObject;
                });
            });
    });
};