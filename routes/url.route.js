/**
 * Created by thedafda on 10/07/15.
 */

var express = require('express'),
    router = express.Router(),
    error = require('../errors/error'),
    baseDomain = process.env.DOMAIN || 'http://localhost:8000/';

function register(db){
    /**
     * @api {post} /url Request Short URL
     * @apiName GetShortUrl
     * @apiGroup Url
     *
     * @apiParam {String} Short to be shortened
     *
     * @apiSuccess {Number} _id Uniqid.
     * @apiSuccess {String} shortUrl  Short URL.
     * @apiSuccess {String} url Original URL
     * @apiSuccess {String} hash Hash.
     */
    router.post('/url', function (req, res, next) {
        var url = req.body && req.body.url;
        if(url){
            db.getShortUrl(url, { baseDomain: baseDomain }).then(function(urlObject){
                res.json(urlObject)
            }).catch(function(err){
                console.log(err);
                next(error.create(500, 'URL_RETRIVE_ERROR', 'Error: ' + err));
            });
            return;
        }
        next(error.create(500, 'INVALID_URL', 'Please pass valid URL'));
    });

    /**
     * @api {get} /url get original url from Short URL
     * @apiName GetLongUrl
     * @apiGroup Url
     *
     * @apiParam {String} Shorted url;
     *
     * @apiSuccess {Number} _id Uniqid.
     * @apiSuccess {String} shortUrl  Short URL.
     * @apiSuccess {String} url Original URL
     * @apiSuccess {String} hash Hash.
     */
    router.get('/url', function (req, res) {
        res.send('Hello World!');
    });

    return router;
}

exports.register = function(db){
    return register(db);
};