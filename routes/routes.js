/**
 * Created by thedafda on 08/07/15.
 */

var Promise = require('bluebird');
var express = require('express');
var bodyParser = require('body-parser');
var error = require('../errors/error');

// Routes
var urlRoute = require('./url.route');
var goRoute = require('./go.route');

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function startServer(app){
    return new Promise(function (resolve, reject) {
        var server = app.listen(process.env.PORT || 8000, function (err) {
            if(err) reject(err);
            resolve(server);
        });
    });
}

function registerUrlRoutes(app, db){

    app.use('/static', express.static('public'));

    app.use(urlRoute.register(db));
    app.use(goRoute.register(db));

    app.get('/', function(req, res){
        res.redirect('/static');
    });

    app.use(function(req, res, next) {
        next(error.create(404, 'Not Found'));
    });

    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        console.log(JSON.stringify(err));
        res.send({
            message: err.message,
            error: err
        });
    });

    /*app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });*/
}

var serverConnect = startServer(app);

exports.registerRoutes = function(db){
    return serverConnect.then(function(server){
        registerUrlRoutes(app, db);
        var host = server.address().address;
        var port = server.address().port;
        console.log('Server started at http://%s:%s', host, port);
        return app;
    });
};