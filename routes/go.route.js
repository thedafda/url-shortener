/**
 * Created by thedafda on 10/07/15.
 */

var express = require('express'),
    router = express.Router(),
    error = require('../errors/error'),
    Base62 = require('../base62/base62');

function register(db){

    /**
     * @api {get} /go/:hash will redirect to original url
     * @apiName GoToUrl
     * @apiGroup Go
     *
     * @apiParam {String} hash Shorted url.
     *
     * @apiSuccess {Number} _id Uniqid.
     * @apiSuccess {String} shortUrl  Short URL.
     * @apiSuccess {String} url Original URL
     * @apiSuccess {String} hash Hash.
     */
    router.get('/go/:hash', function (req, res, next) {
        var base62String = req.params && req.params.hash;
        var id = Number(Base62.decode(base62String));
        if(!isNaN(id)){
            db.getUrlObjectById(id).then(function(urlObject){
                if(urlObject && urlObject.url){
                    res.redirect(urlObject.url);
                } else {
                    next(error.create(401, 'NOT_ABLE_TO_FIND_URL'));
                }

            }).catch(function(err){
                next(error.create(500, 'SHORT_URL', 'Short URL fetching error ' + err));
            });
            return;
        }
        next(error.create(500, 'INVALID_SHORT_URL', 'Link is invalid'));
    });

    return router;
}

exports.register = function(db){
    return register(db);
};