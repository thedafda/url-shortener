module.exports = function(grunt) {

  grunt.initConfig({
    apidoc: {
      app: {
        src: "./",
        dest: "public/apidoc/",
        options: {
          includeFilters: [ ".*\\.js$" ],
          excludeFilters: [ "node_modules/" ]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-apidoc');

  grunt.registerTask('default', ['apidoc:app']);

};