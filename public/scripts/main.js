/**
 * Created by thedafda on 08/07/15.
 */

String.prototype.supplant = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
};

$(function(){

    var tmplt = [
        '<div id="result">',
            '<div class="short">',
                '<a href="{shortUrl}" target="_blank">',
                    '{shortUrl}',
                '</a>',
            '</div>',
            '<div class="long">',
                '<a href="{url}" target="_blank">',
                    '{url}',
                '</a>',
            '</div>',
        '<div>'
    ].join('');

    function arrayToObj(arr){
        var obj = {};
        arr.forEach(function(o){
            obj[o.name] = o.value;
        });
        return obj;
    }

    $('#url-form').on('submit', function(e){
        e.preventDefault();
        var req = arrayToObj($(this).serializeArray() || []);

        $.post('/url', {
            url: req.url
        }).then(function(res){
            $('#result-container').html(tmplt.supplant(res));
        });

        return false;
    });

});