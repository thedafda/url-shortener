# URL Shortner #

https://mpkd.herokuapp.com/

## Technology Stack ##

**Back End**

* Heroku
* NodeJS
* MongoDB
* Bluebird 
* ExpressJS
* Grunt
* apidoc

**Front End**

* jQuery

-------------------------

##Directory Structure##

* ./routes - Routes to register
* ./db - MongoDB Wrapper
* ./error - Error Wrapper
* ./base62 - base62 library
* ./public/* - public facing application code
* ./public/scripts/* - all javascript files
* ./public/styles/* - all styles files


-------------------------

##Commands##

Before executing any of the below commands once run `npm install` in project directory. this will download the required dependencies to the project.

* To install grunt cli globally use `npm install -g grunt-cli` command.
* To start server use `npm start` or `node index.js` command.
* To generate API docs use `grunt apidoc` command.