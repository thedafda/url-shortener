var Base62 = {
    CHARS: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    encode: function(decimal){
        var decimal = decimal || 0, encodedString = '';

        if (decimal === 0){
            return '0';
        }

        while (decimal > 0) {
            encodedString = this.CHARS[decimal % 62] + encodedString;
            decimal = Math.floor(decimal/62);
        }

        return encodedString;
    },
    decode: function(encodedString){
        var scope = this, decimal = 0, encodedStringArr = encodedString.split("").reverse();

        encodedStringArr.forEach(function(char, index){
            decimal += scope.CHARS.indexOf(char) * Math.pow(62, index);
        });

        return decimal;
    }
}


module.exports = Base62;