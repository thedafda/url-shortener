var db = require('./db/db');
var routes = require('./routes/routes');

db.connect().then(function(db) {
    return routes.registerRoutes(db);
}).then(function(){
    console.log('## Application Started ##');
});

